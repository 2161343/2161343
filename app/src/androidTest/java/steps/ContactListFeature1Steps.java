package steps;

import com.mauriciotogneri.greencoffee.GreenCoffeeSteps;
import com.mauriciotogneri.greencoffee.annotations.Given;
import com.mauriciotogneri.greencoffee.annotations.Then;

public class ContactListFeature1Steps extends GreenCoffeeSteps {
    @Given("^I have opened the application$")
    public void i_have_opened_the_application() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Given("^I have contacts$")
    public void i_have_contacts() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^I see the list of contacts’ names$")
    public void i_see_the_list_of_contacts_names() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Given("^I have no contacts$")
    public void i_have_no_contacts() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^I see an empty contact list$")
    public void i_see_an_empty_contact_list() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^I see a message saying that I have no contacts$")
    public void i_see_a_message_saying_that_I_have_no_contacts() {
        // Write code here that turns the phrase above into concrete actions

    }
}
