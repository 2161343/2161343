package features;

import android.support.test.rule.ActivityTestRule;

import com.example.rs.lab3taes.MainActivity;
import com.mauriciotogneri.greencoffee.GreenCoffeeConfig;
import com.mauriciotogneri.greencoffee.GreenCoffeeTest;
import com.mauriciotogneri.greencoffee.ScenarioConfig;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;

import gherkin.cli.Main;
import steps.ContactListFeature1Steps;

@RunWith(Parameterized.class)
public class ContactListFeature1StepsTest extends GreenCoffeeTest {

    @Rule
    public ActivityTestRule<MainActivity> activity = new ActivityTestRule<>(MainActivity.class);

    public ContactListFeature1StepsTest(ScenarioConfig scenario) {
        super(scenario);
    }
    @Parameterized.Parameters(name = "{0}")
    public static Iterable<ScenarioConfig> data() throws IOException {
        return new GreenCoffeeConfig()
                .withFeatureFromAssets("assets/features/ContactListFeature1.feature")
                .scenarios();
    }

    @Test
    public void test()
    {
        start(new ContactListFeature1Steps());
    }
}
